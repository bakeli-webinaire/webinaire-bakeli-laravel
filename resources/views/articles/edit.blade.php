@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Créer un article</h2>
        <div class="row">
            <form action="{{ route('articles.update', $article->id) }}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PATCH">
                @include('articles.edit_fields')
            </form>

        </div>
    </div>
@endsection