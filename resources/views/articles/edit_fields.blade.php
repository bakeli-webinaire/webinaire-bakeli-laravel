<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" placeholder="Article title" value="{{ $article->title }}" class="form-control">
            @if ($errors->has('title'))
                <div class="alert alert-danger">
                    {{ $errors->first('title') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="content">Content</label>
            <textarea type="text" name="content" placeholder="Article content" class="form-control">{{ $article->content }}"</textarea>
            @if ($errors->has('content'))
                <div class="alert alert-danger">
                    {{ $errors->first('content') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Modifier</button> &nbsp;
        <a href="{{ route('articles.index') }}" class="btn btn-danger">Retour</a>
    </div>
</div>