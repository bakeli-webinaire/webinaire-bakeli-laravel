@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h1>{{ $article->title }}</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ $article->content }}
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('articles.index') }}" class="btn btn-danger">Retour</a>
                </div>
            </div>
        </div>
    </section>
@endsection