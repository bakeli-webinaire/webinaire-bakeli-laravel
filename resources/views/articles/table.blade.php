<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Content</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @if ($articles->isEmpty())
            <div class="alert alert-warning">
                Aucune donnée
            </div>
        @else
            @foreach ($articles as $item)
                <tr>
                    <th scope="row"> {{ $item->id }} </th>
                    <td> {{ $item->title }} </td>
                    <td> {{ $item->content }} </td>
                    <td>
                        <a href="{{ route('articles.show', $item->id) }}" class="btn btn-primary">Show</a>
                        <a href="{{ route('articles.edit', $item->id) }}" class="btn btn-warning">Edit</a>
                        <form action="{{ route('articles.destroy', $item->id) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="delete">
                            <button onclick="return confirm('Confirmez-vous la suppression de cet element ?')" type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
              </tr>
            @endforeach
        @endif
      
    </tbody>
</table>