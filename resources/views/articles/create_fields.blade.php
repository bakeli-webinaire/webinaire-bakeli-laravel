<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" placeholder="Article title" value="{{ old('title') }}" class="form-control">
            @if ($errors->has('title'))
                <div class="alert alert-danger">
                    {{ $errors->first('title') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="content">Content</label>
            <textarea type="text" name="content" placeholder="Article content" value="{{ old('content') }}" class="form-control"></textarea>
            @if ($errors->has('content'))
                <div class="alert alert-danger">
                    {{ $errors->first('content') }}
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Ajouter</button> &nbsp;
        <a href="{{ route('articles.index') }}" class="btn btn-danger">Retour</a>
    </div>
</div>