@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Créer un article</h2>
        <div class="row">
            <form action="{{ route('articles.store') }}" method="post">
                @csrf
                @include('articles.create_fields')
            </form>

        </div>
    </div>
@endsection